var console = require('./debug.js')
var editor = require("./keyframeEditor.js");
var mqtt = require("./mqttHandler.js").mqttHandler
var Grbl = require("./grbl.js");
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const XboxController = require('./xboxController.js');

const NODE_RED_CONTROLLER_BASETOPIC = require("./mqttBasetopics.js").NODE_RED_CONTROLLER_BASETOPIC

var controller = new XboxController({
	pid: '045e',
	vid: '028e',
	interval: 50
})



const serialport = new SerialPort('/dev/serial0', {
	baudRate: 115200
})
serialportParser = new Readline()
serialport.pipe(serialportParser)


// setup panTiltSlider
console.log("setup panTiltSlider");
var panTiltSliderInterface = editor.addInterface(new Grbl({
	writeFunction: (data) => {
		serialport.write(data, err => {
			if (err) console.error(err)
		})
		serialport.drain()
	},
	name: "panTiltSlider",
	abbreviation: "pts",
	rxBufferMaxSpace: 128,
	jogIncrementTime: 0.008,
	statusInterval: 20,
	sendBufferInterval: 5,
	maxJogSpeed: 1000,
	// debugInterval: 1000,
	travelToFirstKeyframeSpeed: 5000,
	axes: [{
		axisName: "pan",
		axisLetter: "A",
		internalName: "X"
	}, {
		axisName: "tilt",
		axisLetter: "B",
		internalName: "Y"
	}, {
		axisName: "slide",
		axisLetter: "X",
		internalName: "Z"
	}, ]
}))

// serial setup
console.log("serial setup");
serialportParser.on('data', line => {
	panTiltSliderInterface.read(line)
})

// controller setup
controller.axes["Y1"].on("value", (value) => {
	editor.jog("panTiltSlider", {
		tilt: value
	})
})
controller.axes["X1"].on("value", (value) => {
	editor.jog("panTiltSlider", {
		pan: value
	})
})
controller.joystics["1"].on("center", (value) => {
	console.log("index.js jog cancel");
	editor.jogCancel("panTiltSlider")
})

controller.triggers["LT"].on("value", (value) => {
	if (controller.triggers["RT"].value == 0) {
		editor.jog("panTiltSlider", {
			slide: value * -1
		})
	}
})
controller.triggers["RT"].on("value", (value) => {
	if (controller.triggers["LT"].value == 0) {
		editor.jog("panTiltSlider", {
			slide: value
		})
	}
})
controller.buttons["X"].on("fall", () => {
	editor.killAlarmLock("panTiltSlider")
})
controller.buttons["back"].on("fall", () => {
	editor.setZero("panTiltSlider")
})

controller.buttons["du"].on("fall", () => {
	editor.incTime()
})
controller.buttons["dd"].on("fall", () => {
	editor.decTime()
})
controller.buttons["dl"].on("fall", () => {
	editor.incTimeEditPosition()
})
controller.buttons["dr"].on("fall", () => {
	editor.decTimeEditPosition()
})


mqtt.subscribe(NODE_RED_CONTROLLER_BASETOPIC + "/leftJoystick", (value) => {
	// console.log("leftJoystick");
	value = JSON.parse(value)
	// console.log(value.vector);
	editor.jog("panTiltSlider", {
		tilt: value.vector.y
	})
	editor.jog("panTiltSlider", {
		pan: value.vector.x
	})
})

mqtt.subscribe(NODE_RED_CONTROLLER_BASETOPIC + "/rightJoystick", (value) => {
	// console.log("rightJoystick");
	value = JSON.parse(value)

	// console.log(value.vector);
})

mqtt.subscribe(NODE_RED_CONTROLLER_BASETOPIC + "/leftTrigger", (value) => {
	// console.log("leftTrigger");
	value = JSON.parse(value)

	// console.log(value.vector);
})

mqtt.subscribe(NODE_RED_CONTROLLER_BASETOPIC + "/rightTrigger", (value) => {
	// console.log("rightTrigger");
	value = JSON.parse(value)
	editor.jog("panTiltSlider", {
		slide: value.vector.x
	})

	// console.log(value.vector);
})





// pipe stdin to grbl to send manual commands
var stdinReadline = require('readline');
var rl = stdinReadline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: false
});

rl.on('line', (line) => {
	console.log(">>" + line);
	serialport.write(Buffer.from(line + '\r', 'utf-8'), err => {
		if (err) console.error(err);
	});
})
