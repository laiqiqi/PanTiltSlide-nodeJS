var console = require('./debug.js')
const mqtt = require('mqtt')

const GLOBAL_BASETOPIC = require("./mqttBasetopics.js").GLOBAL_BASETOPIC

class MqttHandler {
	constructor() {
		console.group("setup MqttHandler")
		this.connectCallbacks = [];
		this.subscriptionCallbacks = [];
		this.mqttClient = null
		this.host = 'localhost'
		this.BASETOPIC = GLOBAL_BASETOPIC
		this.connect()
		console.groupEnd()
	}

	connect() {
		console.log("connect MQTT");
		this.mqttClient = mqtt.connect(this.host, {
			port: 1883,
			protocol: "mqtt"
		})
		// Mqtt error calback
		this.mqttClient.on('error', (err) => {
			console.log(err)
			this.mqttClient.end()
		})

		// Connection callback
		this.mqttClient.on('connect', () => {
			console.group('mqtt connected');
			console.info("execute mqtt connectCallbacks");
			this.connectCallbacks.forEach((callback) => {
				callback();
			});
			console.info("mqtt connectCallbacks done");
			console.groupEnd();
		})

		this.mqttClient.on('message', (topic, message) => {
			console.group("mqttclient message event")
			console.log("topic: " + topic)
			console.log("message: " + message)
			if (this.subscriptionCallbacks[topic] != undefined) {
				this.subscriptionCallbacks[topic](message)
			} else {
				console.error("Callback not defined: ");
				console.error("		topic: " + topic);
				console.error("		message: " + message);
			}
			console.groupEnd();
		})

		this.mqttClient.on('close', () => {
			console.log(`mqtt client disconnected`)
		})
	}

	publish(topic, message) {
		if (typeof message != "string") {
			message = message.toString()
		}
		if (!this.mqttClient.connected) {
			this.addConnectCallback(() => {
				this.mqttClient.publish(topic, message)
			})
		} else {
			this.mqttClient.publish(topic, message)
		}
	}

	subscribe(topic, callback) {
		console.log("subscribed: " + topic);
		this.subscriptionCallbacks[topic] = callback
		this.mqttClient.subscribe(topic);
	}

	/**
	 * [addConnectCallback description]
	 * add a callback that will be executed when the mqtt client is connected to
	 * the broker. If the client is already connected, the callback will
	 * be executed immediately
	 * @param {Function} callback  [description]
	 */
	addConnectCallback(callback) {
		if (!this.mqttClient.connected) {
			this.connectCallbacks.push(callback)
		} else {
			callback()
		}
	}

}

var mqttHandler = new MqttHandler()
module.exports.mqttHandler = mqttHandler
