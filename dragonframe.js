var console = require('./debug.js')
var settingStore = require('./mqttSettings.js').settingStore
var functionStore = require('./mqttFunctions.js').functionStore
var statusStore = require('./mqttStatus.js').statusStore


// Hello (hi) - initiate communication
// IN: hi
// OUT: hi <major version> <number of axes> <full version string>
// EXAMPLE: hi 1 8 1.2.7
//
// Motor Status (ms) - check moving status of all motors
// IN: ms
// OUT: ms <0 or 1 for each motor, 1 = moving>
// EXAMPLE: ms 10000000
//
// Move Motor (mm) - move a motor to a position
// IN: mm <motor> <step position>
// EXAMPLE: mm 1 250
// OUT: mm <motor> <target step position>
// OUT2: mp <motor> <step position>
// EXAMPLE: mm 1 250   (if motor needs to move)
// EXAMPLE: mp 1 250   (if motor was already there)
//
// Motor Position Query (mp) - request motor position
// IN: mp <motor>
// OUT: mp <motor> <step position>
//
// Stop Motor (sm) - stop a motor fairly quickly
// IN: sm <motor>
// EXAMPLE: sm 1
// OUT: sm <motor>
// EXAMPLE: sm 1
//
// Stop All (sa) - stop all motors fairly quickly
// IN: sa
// OUT: sa
//
// Jog Motor (jm) - move the motor at a reasonable speed (pulse rate) towards destination
// IN: jm <motor> <destination>
// OUT: jm <motor>
//
// Inch Motor (im) - move the in very small increments towards destination
// IN: im <motor> <destination>
// OUT: im <motor>
//
// Pulse Rate (pr) - set the maximum steps/second of motor
// IN: pr <motor> <steps/second>
// EXAMPLE: pr 1 20000
// OUT: pr <motor> <steps/second>
//
// Zero Motor (zm) - resets the motor's current position to zero (no movement)
// IN: zm <motor>
// OUT: zm <motor>
//
// New Position (np) - sets the motor's current position to a new value (no movement)
// IN: np <motor> <position>
// OUT: np <motor> <position>
