var console = require('./debug.js')
var mqtt = require("./mqttHandler.js").mqttHandler

const STATUS_BASETOPIC = require("./mqttBasetopics.js").STATUS_BASETOPIC

// publishes the set value over mqtt
// the value can not be written over mqtt
// also calls all local callbacks
class StatusStore {
	constructor() {
		this._statuses = [];
	}

	// (name, value, callback, topic)
	addStatus(options) {
		console.group("addMqttStatus: " + options.name + ": " + options.topic);
		if (this._statuses[options.name] == undefined) {
			this._statuses[options.name] = new Status(options)
		} else {
			console.error("Status name is taken: " + options.name);
			return
		}

		console.groupEnd()
		return this._statuses[options.name]
	}

	addCallback(name, callback) {
		try {
			this._statuses[name].addCallback(value)
		} catch (e) {
			console.error(e);
		}
	}

	update(name, value) {
		try {
			this._statuses[name].update(value)
		} catch (e) {
			console.error(e);
		}
	}
}

class Status {
	// (name, value, callback, topic)
	constructor(options) {
		this.name = options.name
		this.value = options.value
		this.callbacks = []

		if (options.topic != undefined) {
			this.basetopic = options.topic
		} else {
			this.basetopic = mqtt.BASETOPIC + "/" + STATUS_BASETOPIC + "/" + this.name
		}

		if (options.callback != undefined) {
			this.addCallback(options.callback)
		}

		if (options.value != undefined) {
			this.update(options.value, true)
		}
	}
	_executeCallbacks() {
		this.callbacks.forEach(callback => {
			callback(this.value)
		});
	}

	update(value, force) {
		if ((value !== this.value) || force) {
			this.value = value
			console.log("[" + this.name + "]" + "Set status: " + JSON.stringify(value));
			this._executeCallbacks();
			mqtt.publish(this.basetopic + "/R", this.value);
		}
	}

	addCallback(callback) {
		this.callbacks.push(callback)
	}
}

var statusStore = new StatusStore();
module.exports.statusStore = statusStore;
