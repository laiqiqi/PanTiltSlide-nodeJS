const process = require('process');
const Gpio = require('pigpio').Gpio;

const led = new Gpio(12, {
	mode: Gpio.OUTPUT
});

var dutyCycle = process.argv[2]
var duration = process.argv[3]
if (dutyCycle == undefined) {
	dutyCycle = 128
}
if (duration == undefined) {
	duration = 500
}
var done = false

var iv = setInterval(() => {
	led.pwmWrite(dutyCycle);
	if (done) {
		led.pwmWrite(0);
		clearInterval(iv);
		process.exit(0)
	}
	done = true;
}, duration);

process.on("SIGINT", (code) => {
	led.pwmWrite(0);
	clearInterval(iv);
})
