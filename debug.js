const LOG = 0
const GROUP = 1
const INFO = 2
const WARN = 3
const ERROR = 4

var FgBlack = "\x1b[30m"
var FgRed = "\x1b[31m"
var FgGreen = "\x1b[32m"
var FgYellow = "\x1b[33m"
var FgBlue = "\x1b[34m"
var FgMagenta = "\x1b[35m"
var FgCyan = "\x1b[36m"
var FgWhite = "\x1b[37m"

var DEBUGLEVEL = LOG

module.exports = {
	LOG: LOG,
	INFO: INFO,
	WARN: WARN,
	ERROR: ERROR,
	setLevel: (level) => {
		DEBUGLEVEL = level
	},
	group: (msg) => {
		if (GROUP >= DEBUGLEVEL) {
			// if (msg != undefined) {
			console.group(FgMagenta, "[GROUP]", FgWhite, msg);
			// } else {
			//     console.group();
			// }
		}
	},
	groupEnd: (msg) => {
		if (GROUP >= DEBUGLEVEL) {
			console.groupEnd();
		}
	},
	log: (msg) => {
		if (LOG >= DEBUGLEVEL) {
			console.log(FgWhite, "[LOG  ]", FgWhite, msg)
			return true
		}
		return false
	},
	info: (msg) => {
		if (INFO >= DEBUGLEVEL) {
			console.info(FgMagenta, "[INFO ]", FgMagenta, msg)
			return true
		}
		return false
	},
	warn: (msg) => {
		if (WARN >= DEBUGLEVEL) {
			console.warn(FgYellow, "[WARN ]", FgYellow, msg)
			return true
		}
		return false
	},
	error: (msg) => {
		if (ERROR >= DEBUGLEVEL) {
			console.error(FgRed, "[ERROR]", FgRed, msg)
			return true
		}
		return false
	},
	trace: (msg) => {
		if (ERROR >= DEBUGLEVEL) {
			console.trace(FgRed, "[ERROR]", FgWhite, msg)
			return true
		}
		return false
	},
	time: (msg) => {
		console.time(msg)
	},
	timeEnd: (msg) => {
		console.timeEnd(msg)
	},
	timeLog: (msg) => {
		console.timeLog(msg)
	}
}
