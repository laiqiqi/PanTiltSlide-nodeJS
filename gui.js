var console = require('./debug.js')
var lcd = require("./lcd.js").lcd;

String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

// combines two strings while retaining position of characters
// every character in stringToCombine that is not a space overrides
// the character at the same position in the calling string
String.prototype.combine = function(stringToCombine) {
    var outString = this
	var l = 0

	if (outString.length > stringToCombine.length) {
		l = outString.length
	} else {
		l = stringToCombine.length
	}

    for (var i = 0; i < l; i++) {
        if (stringToCombine.charAt(i) != undefined) {
            if (stringToCombine.charAt(i) != " ") {
                outString = outString.replaceAt(i, stringToCombine.charAt(i))
            }
        } else {
            // end of stringToCombine
            return outString + stringToCombine.substr(i, stringToCombine.length)
        }
    }
	return outString
}

const ITEM_SPACE_COUNT = 19
const LINE_SPACE_COUNT = ITEM_SPACE_COUNT + 1
const EDIT_CHAR = ">"
const CURSOR_CHAR = ">"
const NUM_LINES = 4

class Item {
    // constructor(name, parent, next, previous) {
    constructor(options) {
        Object.assign(this, options)
    }

    draw() {
        if (this.drawCallback != undefined) {
            this.value = this.drawCallback()
            return (this.name + " " + this.value + Array(ITEM_SPACE_COUNT + 1).join(" ")).substring(0, ITEM_SPACE_COUNT)
        } else {
            return (this.name + Array(ITEM_SPACE_COUNT + 1).join(" ")).substring(0, ITEM_SPACE_COUNT)
        }

    }

    print(space) {
        // console.log(space + this.name);
        if (this.next != undefined) {
            this.next.print(space);
        }
    }
    escape() {
        // console.log(this.name + " escape");
        if (this.menue.position.parent != undefined && this.menue.position.parent != this.menue) {
            this.menue.position = this.menue.position.parent;
            this.menue.frameStart = this.menue.position.parent;
            this.gui.draw(true)
        }
    }
    action() {
        // console.log(this.name + " action");
    }
    up() {
        // console.log(this.name + " up");
    }
    down() {
        // console.log(this.name + " down");
    }
}

class Entry extends Item {
    // constructor(name, parent, next, previous, value, callback, increment, min, max) {
    constructor(options) {
        super(options);
        Object.assign(this, options)
        if (this.increment == undefined) {
            this.increment = 1
        }
    }
}

class Number extends Entry {
    constructor(options) {
        super(options)
        if (this.increment == undefined) {
            this.increment = 1
        }
    }
    action() {
        console.log("Number action " + this.name);
        switch (this.menue.mode) {
            case "navigate":
                this.menue.editUndoSave = this.value
                this.menue.mode = "edit"

                break;
            case "edit":
                this.menue.mode = "navigate"
                this.callback(this.value)
                break;
            default:
        }
        this.gui.draw(true)
    }
    escape() {
        switch (this.menue.mode) {
            case "navigate":
                if (this.menue.position.parent != undefined && this.menue.position.parent != this.menue) {
                    this.menue.position = this.menue.position.parent;
                    this.menue.frameStart = this.menue.position.parent;
                    this.gui.draw(true)
                }
                break;
            case "edit":
                this.value = this.menue.editUndoSave
                this.menue.mode = "navigate"
                break;
            default:
        }
        this.gui.draw(true)
    }
    draw(mode) {
        switch (mode) {
            case "edit":
                return (this.name + Array(ITEM_SPACE_COUNT + 1).join(" ")).substring(0, ITEM_SPACE_COUNT - ((this.value + "").length + 1)) + EDIT_CHAR + this.value
                break;
            case "navigate":
            default:
                if (this.drawCallback != undefined) {
                    this.value = this.drawCallback()
                }
                return (this.name + Array(ITEM_SPACE_COUNT + 1).join(" ")).substring(0, ITEM_SPACE_COUNT - (this.value + "").length) + this.value
                break;
        }
    }
}

class Function extends Item {
    constructor(options) {
        super(options)
    }
    action() {
        this.callback()
        this.gui.draw(true)
    }
}

class Fork extends Item {
    // constructor(name, parent, next, previous) {
    constructor(options) {
        super(options);
        this.head = undefined;
        this.tail = undefined;
        this.row = 1;
        this.index = 0;
        this.itemCount = 0;
        this.mode = "navigate"
        this.editUndoSave = undefined;
        this.items = []
    }

    up() {
        if (this.mode == "edit") {
            this.menue.position.value = "" + (parseInt(this.menue.position.value) + parseInt(this.menue.position.increment));
            this.gui.draw(true);
        } else {
            if (this.menue.position.previous != undefined) {
                this.menue.position = this.menue.position.previous;
                this.gui.draw(true);
            }
        }
    }

    down() {
        if (this.mode == "edit") {
            this.menue.position.value = "" + (parseInt(this.menue.position.value) - parseInt(this.menue.position.increment));
            this.gui.draw(true)
        } else {
            if (this.menue.position.next != undefined) {
                this.menue.position = this.menue.position.next;
                this.gui.draw(true)
            }
        }
    }

    // name, value, callback,
    addElement(item) {
        console.log("addElement to " + this.name + ":" + item.name);
        item.menue = this.menue
        item.gui = this.gui
        item.parent = this
        item.index = this.itemCount

        if (this.menue.head == undefined) {
            console.log("first item of menue");
            this.menue.position = item;
            this.menue.frameStart = item;
        }

        if (this.head == undefined) {
            this.head = item
            this.tail = item;
        } else {
            this.tail.next = item
            item.previous = this.tail
            this.tail = this.tail.next
        }

        this.items.push(item)

        this.itemCount++;
        return item
    }

    addItem(args) {
        console.log("addItem: " + args.name);
        this.addElement(new Item(args))
        return this
    }

    addNumber(args) {
        console.log("addNumber: " + args.name);
        this.addElement(new Number(args))
        return this
    }

    addFunction(args) {
        console.log("addFunction: " + args.name);
        this.addElement(new Function(args))
        return this
    }

    addFork(args) {
        console.log("addFork: " + args.name);
        return this.addElement(new Fork(args))
    }

    print(space) {
        if (space == undefined) {
            space = "";
        }
        if (this.name != undefined) {
            console.log(space + this.name)
        }
        if (this.head != undefined) {
            this.head.print(" " + space);
        }
        if (this.next != undefined) {
            this.next.print("" + space);
        }
    }

    action() {
        console.log(this.name + " action");
        if (this.head != undefined) {
            console.log("enter fork");
            this.menue.position = this.head
            this.menue.frameStart = this.head
            this.gui.draw(true)
        }
    }
}

class Menue extends Fork {
    // constructor(name, parent, next, previous) {
    constructor(options) {
        // lcd.clear()
        super(options)
        this.menue = this
        this.position = undefined;
        this.frameStart = undefined;
        this.cursorRow = 0
    }
    escape() {
        this.position.escape()
    }

    action() {
        console.log(this.position.name);
        this.position.action()
    }

    draw() {
        var drawbuffer = [
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" ")
        ]

        if (((this.frameStart.index + NUM_LINES - 1) >= this.position.index) || ((this.frameStart.index) < this.position.index)) {
            // position is out of frame
            if ((this.frameStart.index + NUM_LINES - 1) < this.position.index) {
                // position exeedes lower bounds
                this.frameStart = this.position.parent.items[this.position.index - (NUM_LINES - 1)]
            }
            if (this.frameStart.index >= this.position.index) {
                // position exeedes upper bounds
                this.frameStart = this.position
            }
        }

        this.cursorRow = this.position.index - this.frameStart.index
        for (var i = 0; i < 4; i++) {
            if (this.position.parent.items[this.frameStart.index + i] == undefined) {
                break
            }
            if (i == this.cursorRow) {
                if (this.mode == "edit") {
                    drawbuffer[i] = " " + this.position.parent.items[this.frameStart.index + i].draw(this.mode)
                } else {
                    drawbuffer[i] = CURSOR_CHAR + this.position.parent.items[this.frameStart.index + i].draw(this.mode)
                }
            } else {
                drawbuffer[i] = " " + this.position.parent.items[this.frameStart.index + i].draw()
            }
        }
        return drawbuffer
    }
}

class Screen {
    // enterCallback
    // leaveCallback
    constructor(options) {
        Object.assign(this, options)
    }
}

class Gui {
    constructor(options) {
        this.pages = []
        this.pageIndexCounter = 0
        this.position = undefined
        this.sendbuffer = ["", "", "", ""]
        this.dirty = false;
        this.refreshIntervall = setInterval(() => {
            if (this.dirty) {
                this.dirty = false;
                lcd.print(this.sendbuffer.join('\n'))
            }
        }, 300)
    }

    static getBlankScreen() {
        return [
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" "),
            Array(LINE_SPACE_COUNT + 1).join(" ")
        ]
    }

	// replaces characters in background with characters in overlay if they
	// are not a space
	// background and overlay are arrays of strings which fill the entire drawbuffer
	overlayScreen(background, overlay){
		background.forEach((line, i) => {
			background[i] = line.combine(overlay[i])
		});
		return background;
	}

    draw(force) {
        if (force == undefined) {
            force = false
        }
        if (this.position != undefined) {
            this.sendbuffer = this.position.draw()
            // console.log(this.sendbuffer);
            if (force) {
                lcd.print(this.sendbuffer.join('\n'))
            } else {
                this.dirty = true
            }
        }
    }

    addPage(page) {
        page.gui = this
        page.index = this.pageIndexCounter
        this.pages.push(page)
        if (this.position == undefined) {
            this.position = page
            if (this.position.enterCallback != undefined) {
                this.position.enterCallback()
            }
        }
        this.pageIndexCounter++
        return page
    }

    up() {
        if (this.position != undefined) {
            if (this.position.up != undefined) {
                this.position.up()
            }
        }
    }
    down() {
        if (this.position != undefined) {
            if (this.position.down != undefined) {
                this.position.down()
            }
        }
    }
    escape() {
        if (this.position != undefined) {
            if (this.position.escape != undefined) {
                this.position.escape()
            }
        }
    }
    action() {
        if (this.position != undefined) {
            if (this.position.action != undefined) {
                this.position.action()
            }
        }
    }

    nextPage() {
        // console.log("nextPage");
        if (this.position != undefined) {
            var nextPage = undefined
            if (this.pages[this.position.index + 1] != undefined) {
                nextPage = this.pages[this.position.index + 1]
            } else if (this.position.index != 0) {
                nextPage = this.pages[0]
            }
            if (nextPage != undefined) {
                if (this.position.leaveCallback != undefined) {
                    this.position.leaveCallback()
                }
                this.position = nextPage
                if (this.position.enterCallback != undefined) {
                    this.position.enterCallback()
                }
                lcd.clear()
                this.draw()
            }
        }
    }

    previousPage() {
        // console.log("previousPage");
        if (this.position != undefined) {
            var previousPage = undefined
            if (this.pages[this.position.index - 1] != undefined) {
                previousPage = this.pages[this.position.index - 1]
            }
            if (this.position.index == 0) {
                previousPage = this.pages[this.pageIndexCounter - 1]
            }
            if (previousPage != undefined) {
                if (this.position.leaveCallback != undefined) {
                    this.position.leaveCallback()
                }
                this.position = previousPage
                if (this.position.enterCallback != undefined) {
                    this.position.enterCallback()
                }
                lcd.clear()
                this.draw()
            }
        }
    }

    addMenue(options) {
        console.log("addMenue: " + options.name);
        return this.addPage(new Menue(options))
    }
    addScreen(options) {
        console.log("addScreen: " + options.name);
        return this.addPage(new Screen(options))
    }
}

module.exports = Gui;
