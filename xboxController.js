// prerequisites:
// sudo apt-get install xboxdrv libusb-1.0-0 libusb-1.0-0-dev libudev-dev
//
// place udev rule files in /etc/udev/rules.d/
//
// blacklist xpad driver
// /etc/modprobe.d $ cat blacklist.conf
// blacklist xpad

const EventEmitter = require('events');
var spawn = require('child_process').spawn;

var axesNames = [
	"X1",
	"Y1",
	"X2",
	"Y2",
]

var joysticNames = [
	"1",
	"2"
]

var triggerNames = [
	"LT",
	"RT"
]

var buttonNames = [
	"du",
	"dd",
	"dl",
	"dr",
	"back",
	"guide",
	"start",
	"TL",
	"TR",
	"A",
	"B",
	"X",
	"Y",
	"LB",
	"RB"
]

class Trigger extends EventEmitter {
	constructor(options) {
		super()
		Object.assign(this, options)
		this.value = 0
		this.rawValue = 0;
		if (options.maxPosVal == undefined) {
			this.maxPosVal = 255;
		}
		if (options.deadzone == undefined) {
			this.deadzone = 0;
		}
	}

	update(newValue) {
		if (!Number.isInteger(newValue)) {
			return
		}
		if (this.rawValue != newValue) {
			var scaledValue = this.scale(newValue)
			if (scaledValue != this.value) {
				this.value = scaledValue
				this.rawValue = newValue

				this.emit("value", this.value, this.rawValue)
			}
		}
	}

	scale(value) {
		if (value > this.deadzone) {
			return ((value - this.deadzone) / (this.maxPosVal - this.deadzone)).toFixed(2)
		}
		return 0
	}
}

class Axis extends EventEmitter {
	constructor(options) {
		super()
		Object.assign(this, options)
		this.value = 0
		this.rawValue = 0;
		if (options.deadzone == undefined) {
			this.deadzone = 0;
		}
		if (options.maxPosVal == undefined) {
			this.maxPosVal = 32767;
		}
		if (options.maxNegVal == undefined) {
			this.maxNegVal = -32767;
		}
	}

	update(newValue) {
		if (!Number.isInteger(newValue)) {
			return
		}
		if (this.rawValue != newValue) {

			var scaledValue = this.scale(newValue)
			if (scaledValue != this.value) {
				this.value = scaledValue
				this.rawValue = newValue
				if (this.value == 0) {
					this.emit("center")
				} else {
					this.emit("value", this.value, this.rawValue)
				}
			}
		}
	}

	scale(value) {
		var returnValue = 0
		if (value >= 0) {
			if (value >= this.deadzone) {
				returnValue = (value - this.deadzone) / (this.maxPosVal - this.deadzone)
			} else {
				returnValue = 0;
			}
		} else {
			if (value <= -1 * this.deadzone) {
				returnValue = (value + this.deadzone) / (this.maxNegVal + this.deadzone) * -1
			} else {
				returnValue = 0;
			}
		}
		returnValue = returnValue.toFixed(2);
		return returnValue;
	}
}

class Button extends EventEmitter {
	constructor(options) {
		super()
		Object.assign(this, options)
		this.value = 0
	}

	update(newValue) {
		if (!Number.isInteger(newValue)) {
			return
		}
		if (this.value != newValue) {
			switch (newValue) {
				case 0:
					this.emit("rise")
					break;
				case 1:
					this.emit("fall")
					break;
				default:
					console.error("XBOX BUTTON " + this.name + ": UNRECOGNIZED STATE: " + newValue);
			}
			this.value = newValue
		}
	}
}

class Joystic extends EventEmitter {
	constructor(options) {
		super()
		Object.assign(this, options)
		this.xAxis.on("center", () => {
			this.checkJoystickCenter()
		})
		this.yAxis.on("center", () => {
			this.checkJoystickCenter()
		})
	}

	// emmit center event if both axes of a joystic are centered
	checkJoystickCenter() {
		if (this.xAxis.value == 0 && this.yAxis.value == 0) {
			this.emit("center")
		}
	}
}


class XboxController {
	constructor(options) {
		this.axes = []
		this.buttons = []
		this.triggers = []
		this.joystics = []

		// call xboxdrv
		this.args = ['--device-by-id=' + options.pid + ':' + options.vid, "--type=" + 'xbox360', "--no-uinput"];
		this.xboxdrv = spawn('xboxdrv', this.args);

		if (options.interval == undefined) {
			options.interval = 20
		}

		this.interval = options.interval
		this.readTimestamp = Date.now()

		// setup of all inputs
		axesNames.forEach((name) => {
			this.axes[name] = new Axis({
				name: name,
				deadzone: 20000
			})
		});

		buttonNames.forEach((name) => {
			this.buttons[name] = new Button({
				name: name
			})
		});

		triggerNames.forEach((name) => {
			this.triggers[name] = new Trigger({
				name: name,
				deadzone: 25
			})
		});

		joysticNames.forEach((name) => {
			this.joystics[name] = new Joystic({
				name: name,
				xAxis: this.axes["X" + name],
				yAxis: this.axes["Y" + name]
			})
		});


		// handle xboxdrv data
		this.xboxdrv.stdout.on('data', (data) => {
			// only read controller data in intervals
			if (Date.now() > (this.readTimestamp + this.interval)) {
				this.readTimestamp = Date.now()
				this.readControllerData(data)
			}
		});

		// handle xboxdrv error
		this.xboxdrv.stderr.on('data', function(data) {
			console.error('stderr: ' + data);
		});

		// handle xboxdrv close
		this.xboxdrv.on('close', function(code) {
			console.log('child process exited with code ' + code);
		});

		// add debug callbacks to all EventEmitter
		if (options.debug) {
			Object.keys(this.buttons).forEach((name) => {
				this.buttons[name].on("rise", () => {
					console.log("Button " + name + " rise");
				})
				this.buttons[name].on("fall", () => {
					console.log("Button " + name + " fall");
				})
			});

			Object.keys(this.axes).forEach((name) => {
				this.axes[name].on("center", () => {
					console.log("Axis " + name + " center");
				})
				this.axes[name].on("value", (value, rawValue) => {
					console.log("Axis " + name + " value: " + value + " " + rawValue);
				})
			});

			Object.keys(this.joystics).forEach((name) => {
				this.joystics[name].on("center", () => {
					console.log("Joystic " + name + " center");
				})
			});

			Object.keys(this.triggers).forEach((name) => {
				this.triggers[name].on("value", (value, rawValue) => {
					console.log("Trigger " + name + " value: " + value + " " + rawValue);
				})
			});
		}
	}

	// parsing of the raw output from xboxdrv
	readControllerData(data) {
		var dataArray = data.toString().replaceAll(':', ' ').replace(/ +/g, " ").trim().split(' ')
		var inputArray = []
		for (var i = 0; i < dataArray.length; i++) {
			inputArray[dataArray[i]] = dataArray[++i]
		}

		buttonNames.forEach((name) => {
			this.buttons[name].update(parseInt(inputArray[name]));
		});

		axesNames.forEach((name) => {
			this.axes[name].update(parseInt(inputArray[name]));
		});

		triggerNames.forEach((name) => {
			this.triggers[name].update(parseInt(inputArray[name]));
		});
	}
}

module.exports = XboxController
